package com.msecilmis.revoluttest

import android.graphics.*
import android.widget.EditText
import android.widget.ImageView
import com.mynameismidori.currencypicker.ExtendedCurrency
import com.squareup.picasso.Picasso
import com.squareup.picasso.Transformation
import java.text.DecimalFormat


var map: Map<String, Int>? = null

fun ImageView.loadImage(currency: Currency) {
    map?.get(currency.shortName)?.let {
        Picasso.get().load(it).transform(object: Transformation{
            override fun transform(source: Bitmap): Bitmap? {
                val size = Math.min(source.width, source.height)
                val x = (source.width - size) / 2
                val y = (source.height - size) / 2
                val squaredBitmap = Bitmap.createBitmap(source, x, y, size, size)
                if (squaredBitmap != source) {
                    source.recycle()
                }
                val bitmap = Bitmap.createBitmap(size, size, source.config)
                val canvas = Canvas(bitmap)
                val paint = Paint()
                val shader = BitmapShader(
                    squaredBitmap,
                    Shader.TileMode.CLAMP, Shader.TileMode.CLAMP
                )
                paint.setShader(shader)
                paint.setAntiAlias(true)
                val r = size / 2f
                canvas.drawCircle(r, r, r, paint)
                squaredBitmap.recycle()
                return bitmap
            }

            override fun key(): String? {
                return "circle"
            }
        }).into(this)
    }
}

fun EditText.setFormattedAmount(amount: Double) {
    if (amount == 0.0) {
        setText("")
        return
    }

    val df = DecimalFormat("###.##")
    setText(df.format(amount))
}

fun String.getNormalizedDouble(): Double {
    var normalizedString = this
    if (this.isNullOrBlank()) {
        normalizedString = "0"
    }

    //edgecase, input as .
    val normalizedDouble = try {
        normalizedString.toDouble()
    } catch (e: Exception) {
        0.0
    }

    return normalizedDouble
}

fun Double.format(digits: Int) = "%.${digits}f".format(this)

fun initFlags() {
    map = ExtendedCurrency.CURRENCIES.map {
        var code = it.code ?: "undefined"
        code to it.flag
    }.toMap()
}