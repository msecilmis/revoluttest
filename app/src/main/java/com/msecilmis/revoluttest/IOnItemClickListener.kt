package com.msecilmis.revoluttest

interface IOnItemClickListener {
    fun onItemClick(item: Currency)
}