package com.msecilmis.revoluttest

import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

interface RestInterface {

    @GET("/api/android/latest")
    fun getRatesFor(@Query("base") code: String): Call<Response>
}