package com.msecilmis.revoluttest

data class Currency constructor(
    val shortName: String = "USD",
    val rate: Float = 1f
) {
    var fullName: String = java.util.Currency.getInstance(shortName).displayName

    override fun toString(): String {
        return "Currency(shortName='$shortName', rate=$rate)"
    }


}