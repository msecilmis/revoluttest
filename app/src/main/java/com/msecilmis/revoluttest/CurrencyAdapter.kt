package com.msecilmis.revoluttest

import android.text.Editable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView

class CurrencyAdapter : RecyclerView.Adapter<CurrencyAdapter.ViewHolder>() {
    var textWatcher: TextWatcherWrapper = object : TextWatcherWrapper() {
        override fun afterTextChanged(s: Editable?) {
            val newAmount: Double = s.toString().getNormalizedDouble()

            if (newAmount != amountToCalculate) {
                amountToCalculate = newAmount
            }
        }
    }

    var onAmountChangedListener: IOnAmountChangedListener? = null
    var onItemClickListener: IOnItemClickListener? = null

    var isClickEnabled = true

    var currencyList: List<Currency> = listOf()
        set(value) {
            val diffCallback = CDiffUtilCallback(field, value)
            val diffResult = DiffUtil.calculateDiff(diffCallback, false)
            diffResult.dispatchUpdatesTo(this)

            if (selectedCurrency == value[0].shortName){
                isClickEnabled = true
            }

            selectedCurrency = value[0].shortName
            field = value
        }

    var selectedCurrency: String = ""

    var amountToCalculate: Double = 100.0
        set(value) {
            onAmountChangedListener?.onAmountChanged(value)
            field = value
        }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val itemView = LayoutInflater.from(parent.context)
            .inflate(R.layout.layout_currency_item, parent, false)
        return ViewHolder(itemView)
    }

    override fun getItemCount(): Int = currencyList.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int, payloads: MutableList<Any>) {
        if (payloads.isEmpty()) { // Perform a full update
            super.onBindViewHolder(holder, position, payloads)
        } else { // Perform a partial update
            for (payload in payloads) {
                if (payload is AmountChangePayload) {
                    bindAmountPayload(holder, payload.amount * currencyList[position].rate)
                } else if (payload is ListChangePayload) {
                    bindChangePayload(holder, payload)
                }
            }
        }
    }

    fun bindAmountPayload(holder: ViewHolder, amount: Double) {
        bindResult(holder, amount)
    }

    fun bindChangePayload(holder: ViewHolder, listChangePayload: ListChangePayload) {
        val newData = listChangePayload.newItem
        val oldData = listChangePayload.oldItem
        val isEditable = selectedCurrency == newData.shortName

        if (newData.shortName != oldData.shortName) {
            bindNames(holder, newData)
        }

        if (newData.rate != oldData.rate) {
            bindResult(holder, amountToCalculate * newData.rate)
        }

        bindCurrency(holder, newData, isEditable)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val currency = currencyList[position]
        val isEditable = position == 0

        bindNames(holder, currency)
        bindCurrency(holder, currency, isEditable)

    }

    private fun bindCurrency(holder: ViewHolder, newData: Currency, isEditable: Boolean) {
        holder.itemView.setOnClickListener {
            if (isClickEnabled) {
                isClickEnabled = false
                if (newData.rate != 0f) {
                    onItemClickListener?.onItemClick(newData)
                }
            }
        }

        holder.amount.removeTextChangedListener(textWatcher)

        holder.amount.isEnabled = isEditable
        holder.amount.visibility = if (isEditable) View.VISIBLE else View.GONE
        holder.result.visibility = if (isEditable) View.GONE else View.VISIBLE

        if (isEditable) {
            holder.amount.setFormattedAmount(amountToCalculate)
            holder.result.setText("")
        } else {
            bindResult(holder, amountToCalculate * newData.rate)
            holder.amount.setText("")
        }

        holder.amount.addTextChangedListener(textWatcher)
    }

    private fun bindResult(
        holder: ViewHolder,
        amount: Double
    ) {
        holder.result.setFormattedAmount(amount)
    }

    private fun bindNames(holder: ViewHolder, newData: Currency) {
        holder.shortName.text = newData.shortName
        holder.fullName.text = newData.fullName
        holder.flag.loadImage(newData)
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val flag: ImageView = itemView.findViewById(R.id.flag)
        val shortName: TextView = itemView.findViewById(R.id.shortName)
        val fullName: TextView = itemView.findViewById(R.id.fullName)
        val amount: EditText = itemView.findViewById(R.id.amount)
        val result: EditText = itemView.findViewById(R.id.result)
    }

}