package com.msecilmis.revoluttest

interface IOnAmountChangedListener {
    fun onAmountChanged(amount: Double)
}