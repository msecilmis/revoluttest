package com.msecilmis.revoluttest

import androidx.recyclerview.widget.DiffUtil

class CDiffUtilCallback(val oldList: List<Currency>, val newList: List<Currency>) :
    DiffUtil.Callback() {
    override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        return oldList[oldItemPosition].shortName == newList[newItemPosition].shortName
    }

    override fun getOldListSize(): Int = oldList.size

    override fun getNewListSize(): Int = newList.size

    override fun areContentsTheSame(
        oldItemPosition: Int,
        newItemPosition: Int
    ): Boolean {
        val oldCurr = oldList[oldItemPosition]
        val newCurr = newList[newItemPosition]

        return (oldCurr.shortName == newCurr.shortName
                && oldCurr.rate == newCurr.rate)
    }

    override fun getChangePayload(oldItemPosition: Int, newItemPosition: Int): Any? {
        return ListChangePayload(oldList[oldItemPosition], newList[newItemPosition])
    }
}