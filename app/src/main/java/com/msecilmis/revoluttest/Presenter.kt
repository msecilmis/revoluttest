package com.msecilmis.revoluttest

import kotlinx.coroutines.*
import retrofit2.Call
import retrofit2.Callback

class Presenter {
    private var view: View? = null

    var selectedCurrency = ""
        set(value) {
            field = value
            job?.cancel()
            job = GlobalScope.launch {
                while (true) {
                    withContext(Dispatchers.Main) {
                        getData(selectedCurrency)
                    }
                    delay(1000)
                }
            }
        }

    var job: Job? = null

    fun onCreate(view: View) {
        this.view = view

        selectedCurrency = "EUR"
    }

    fun onDestroy() {
        view = null
    }

    fun getData(code: String) {
        NetworkService.restInterface.getRatesFor(code).enqueue(object : Callback<Response> {
            override fun onResponse(call: Call<Response>, response: retrofit2.Response<Response>) {
                val body = response.body()
                val elements = body?.toCurrencyList() ?: mutableListOf()

                if (elements.size <= 1) {
                    view?.onDataLoadError(R.string.error_message)
                    return
                }

                view?.onDataLoaded(elements)
            }

            override fun onFailure(call: Call<Response>, t: Throwable) {
                view?.onDataLoadError(R.string.error_message)
            }

        })
    }

    interface View : IOnAmountChangedListener, IOnItemClickListener {
        fun onDataLoaded(data: MutableList<Currency>)
        fun onDataLoadError(errRes: Int)
    }
}