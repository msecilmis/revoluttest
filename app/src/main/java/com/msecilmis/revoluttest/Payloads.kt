package com.msecilmis.revoluttest

data class AmountChangePayload(val amount: Double)

data class ListChangePayload(val oldItem: Currency, val newItem: Currency)