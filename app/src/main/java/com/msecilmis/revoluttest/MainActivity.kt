package com.msecilmis.revoluttest

import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.RecyclerView

class MainActivity : AppCompatActivity(), Presenter.View {

    lateinit var currencies: RecyclerView
    private val currencyAdapter = CurrencyAdapter()

    var presenter = Presenter()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        initFlags()
        currencies = findViewById(R.id.currencies)
        currencies.adapter = currencyAdapter
        currencyAdapter.onAmountChangedListener = this
        currencyAdapter.onItemClickListener = this

        presenter.onCreate(this)
    }

    override fun onDestroy() {
        super.onDestroy()
        presenter.onDestroy()
    }

    override fun onDataLoaded(data: MutableList<Currency>) {
        currencyAdapter.currencyList = data
    }

    override fun onDataLoadError(errRes: Int) {
        Toast.makeText(this, getString(errRes), Toast.LENGTH_SHORT).show()
    }

    override fun onAmountChanged(amount: Double) {
        currencies.post {
            currencyAdapter.notifyItemRangeChanged(
                0,
                currencyAdapter.itemCount,
                AmountChangePayload(amount)
            )
        }
    }

    override fun onItemClick(item: Currency) {
        currencyAdapter.amountToCalculate *= item.rate
        presenter.selectedCurrency = item.shortName
    }

}
