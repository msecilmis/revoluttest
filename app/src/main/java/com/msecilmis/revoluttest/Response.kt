package com.msecilmis.revoluttest

import com.google.gson.annotations.SerializedName

data class Response(
    @SerializedName("baseCurrency")
    val baseCurrency: String,
    @SerializedName("rates")
    val rates: LinkedHashMap<String, Float>? = null
) {
}

fun Response.toCurrencyList(): MutableList<Currency> {
    val currencyList: MutableList<Currency> = mutableListOf()
    rates?.forEach {
        currencyList.add(
            Currency(
                shortName = it.key,
                rate = it.value
            )
        )
    }
    currencyList.add(
        0,
        Currency(
            shortName = baseCurrency,
            rate = 1f
        )
    )
    return currencyList
}